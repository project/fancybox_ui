<?php

namespace Drupal\fancybox_ui\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the fancybox_ui.
 *
 * @FieldFormatter(
 *  id = "fancybox_image",
 *  label = @Translation("Fancybox"),
 *  field_types = {"image"}
 * )
 */
class FancyboxImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'image_style' => '',
      'show_caption' => false,
      'wrapper_class' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element = [];

    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );

    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#options' => $image_styles,
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#description' => $description_link->toRenderable()
    ];

    $element['show_caption'] = [
      '#title' => $this->t('Show caption'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('show_caption'),
      '#description' => $this->t('Use alt attribute to show a caption under the content')
    ];

    $element['wrapper_class'] = [
      '#title' => $this->t('Wrapper class'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('wrapper_class'),
      '#description' => $this->t('Custom wrapper class')
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];

    $image_styles = image_style_options(FALSE);

    unset($image_styles['']);

    $select_style = $this->getSetting('image_style');
    $show_caption = $this->getSetting('show_caption');

    if (isset($image_styles[$select_style])) {
      $summary[] = $this->t('URL for image style: @style', ['@style' => $image_styles[$select_style]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }

    if($show_caption) {
     $summary[] = $this->t('Show image caption');
   }

   return $summary;
 }


    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode) {
      $elements = [];
      $other_data = [];

      $show_caption = $this->getSetting('show_caption');
      $wrapper_class = $this->getSetting('wrapper_class');

      $group = '';

      $view_id = \Drupal::routeMatch()->getParameter('view_id');
      if($view_id) {
        $route = \Drupal::routeMatch()->getRouteObject();
        $display_id = $route->getDefault('display_id');

        $group = 'gallery-'.$display_id;
      }

      /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
      if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
        return $elements;
      }

      $entity = $items->getEntity();

      if($group == '') {
        $bundle = $entity->bundle();
        $group = 'gallery-'.$bundle;
      }

      $other_data['group'] = $group;
      $other_data['wrapper_class'] = $wrapper_class;

      $image_style_setting = $this->getSetting('image_style');

      /** @var \Drupal\image\ImageStyleInterface $image_style */
      $image_style = $this->imageStyleStorage->load($this->getSetting('image_style'));
      /** @var \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator */
      $file_url_generator = \Drupal::service('file_url_generator');
      /** @var \Drupal\file\FileInterface[] $images */
      foreach ($images as $delta => $image) {
       $caption = '';
       $image_uri = $image->getFileUri();

       $image_uri = $file_url_generator->generateString($image_uri);

      // Add cacheability metadata from the image and image style.
       $cacheability = CacheableMetadata::createFromObject($image);
       if ($image_style) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
      }

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $image->_referringItem;
      $item_attributes = $item->_attributes;

      if($show_caption) {
        $values = $item->getValue();
        $caption = $values['alt']??'';
        $other_data['caption'] = $caption;
      }

      unset($item->_attributes);

      $elements[$delta] = [
        '#theme' => 'fancybox_image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#image_uri' => $image_uri,
        '#other_data' => $other_data,
      ];

      $cacheability->applyTo($elements[$delta]);

    }
    return $elements;
  }


  /**
   * {@inheritdoc}
   */
  protected function isBackgroundImageDisplay() {
    return $this->getPluginId() == 'fancybox_ui';
  }

}
